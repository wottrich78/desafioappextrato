package github.com.wottrich.desafioappextrato;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import github.com.wottrich.desafioappextrato.API.APIService;
import github.com.wottrich.desafioappextrato.models.Data;
import github.com.wottrich.desafioappextrato.models.User.User;
import github.com.wottrich.desafioappextrato.models.User.UserDes;
import github.com.wottrich.desafioappextrato.models.installment.Detail;
import github.com.wottrich.desafioappextrato.models.installment.Installment;
import github.com.wottrich.desafioappextrato.models.installment.InstallmentAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity {

    private static final String GIT = "HomeActivity";

    private TextView tvName;
    private TextView tvSaldo;
    private TextView tvLimite;
    private TextView tvUtilizado;
    private ListView lvInstallments;
    private ProgressBar progress;


    private InstallmentAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setTitle("Bem-Vindo!");

        tvName = (TextView) findViewById(R.id.tv_name);
        tvSaldo = (TextView) findViewById(R.id.tv_saldo);
        tvLimite = (TextView) findViewById(R.id.tv_limite);
        tvUtilizado = (TextView) findViewById(R.id.tv_utilizado);
        lvInstallments = (ListView) findViewById(R.id.list_installments);
        progress = (ProgressBar) findViewById(R.id.progressBar);

        progress.setVisibility(View.GONE);


        String codigo = getIntent().getStringExtra("codigo");

        if (!codigo.isEmpty()){
            progress.setVisibility(View.VISIBLE);
            getUser(codigo);
        }
        else {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            Intent it = new Intent(HomeActivity.this, LoginActivity.class);
            startActivity(it);
        }

    }

    public void getUser(String codigo){

        Gson g = new GsonBuilder()
                .registerTypeAdapter(User.class, new UserDes()).create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(g))
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<User> callUser = service.getUser(codigo);

        callUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(GIT, "CallBack result:" + call +" / "+ response);
                if(!response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),
                            "error",
                            Toast.LENGTH_SHORT).show();
                    Log.e(GIT, "CallBack result if:" + response.isSuccessful());
                }
                else{
                    final User u = response.body();

                    if (u.getData() == null){
                        Toast.makeText(
                                getApplicationContext(),
                                "Usuario não registrado",
                                Toast.LENGTH_SHORT).show();
                        Intent it = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(it);
                    }
                    else{
                        tvName.setText(u.getData().getName());
                        tvSaldo.setText(u.getData().getLimits().getAvailable());
                        tvLimite.setText(u.getData().getLimits().getTotal());
                        tvUtilizado.setText(u.getData().getLimits().getExpent());

                            progress.setVisibility(View.GONE);
                        for (int i = 0; i < u.getData().getInstallments().size(); i++){
                            final ArrayList<Installment> insAdapter =
                                    new ArrayList<>(u.getData().getInstallments());
                            adapter = new InstallmentAdapter(HomeActivity.this, insAdapter);
                            lvInstallments.setAdapter(adapter);

                            lvInstallments.setOnItemClickListener(
                                    new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view,
                                                        int position, long id) {

                                    final Installment inst = insAdapter.get(position);

                                    AlertDialog.Builder msg =
                                            new AlertDialog.Builder(HomeActivity.this);
                                    msg.setTitle("Informações");
                                    msg.setMessage(inst.getDetail().toString());
                                    msg.setNeutralButton("ok", null);
                                    msg.show();

                                }
                            });
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error: " + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
                Log.e(GIT, "Error: " + t.getMessage());
            }
        });
    }
}
