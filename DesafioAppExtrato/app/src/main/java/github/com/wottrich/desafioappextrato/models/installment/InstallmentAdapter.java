package github.com.wottrich.desafioappextrato.models.installment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import github.com.wottrich.desafioappextrato.R;

/**
 * Created by Note Lenovo on 22/09/2017.
 */

public class InstallmentAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Installment> installments;

    public InstallmentAdapter(Context context, ArrayList<Installment> installments) {
        this.context = context;
        this.installments = installments;
    }

    @Override
    public int getCount() {
        return installments.size();
    }

    @Override
    public Object getItem(int position) {
        return installments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        Installment ins = installments.get(position);

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.installment_adapter, null);
        }

        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
        TextView tvCarnet = (TextView) convertView.findViewById(R.id.tv_carnet);
        TextView tvInstallment = (TextView) convertView.findViewById(R.id.tv_installment);
        TextView tvValue = (TextView) convertView.findViewById(R.id.tv_value);

        tvDate.setText(ins.getPast_due());
        tvCarnet.setText(ins.getCarnet());
        tvInstallment.setText(ins.getInstallment());
        tvValue.setText(ins.getValue());


        return convertView;
    }
}
