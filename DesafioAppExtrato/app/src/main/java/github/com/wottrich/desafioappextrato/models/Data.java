package github.com.wottrich.desafioappextrato.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import github.com.wottrich.desafioappextrato.models.installment.Installment;

/**
 * Created by LucasWottrich on 21/09/2017.
 */

public class Data implements Serializable {


    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("total_overdue")
    @Expose
    private String total_overdue;

    @SerializedName("total_due")
    @Expose
    private String total_due;

    @SerializedName("installments")
    @Expose
    private List<Installment> installments;

    private Limits limits;

    public Data() {
    }

    public Data(String name, String total_overdue, String total_due,
                List<Installment> installments, Limits limits) {
        this.name = name;
        this.total_overdue = total_overdue;
        this.total_due = total_due;
        this.installments = installments;
        this.limits = limits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal_overdue() {
        return total_overdue;
    }

    public void setTotal_overdue(String total_overdue) {
        this.total_overdue = total_overdue;
    }

    public String getTotal_due() {
        return total_due;
    }

    public void setTotal_due(String total_due) {
        this.total_due = total_due;
    }

    public List<Installment> getInstallments() {
        return installments;
    }

    public void setInstallments(List<Installment> installments) {
        this.installments = installments;
    }

    public Limits getLimits() {
        return limits;
    }

    public void setLimits(Limits limits) {
        this.limits = limits;
    }

    @Override
    public String toString() {
        return "Data{" +
                "name='" + name + '\'' +
                ", total_overdue='" + total_overdue + '\'' +
                ", total_due='" + total_due + '\'' +
                ", installments=" + installments +
                ", limits=" + limits +
                '}';
    }
}
