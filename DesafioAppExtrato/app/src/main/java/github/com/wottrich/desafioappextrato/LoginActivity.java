package github.com.wottrich.desafioappextrato;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import github.com.wottrich.desafioappextrato.API.APIService;
import github.com.wottrich.desafioappextrato.Util.Validacao;
import github.com.wottrich.desafioappextrato.models.User.User;
import github.com.wottrich.desafioappextrato.models.User.UserDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private static final String GIT = "Login";

    private EditText etCodigo;
    private EditText etConfirmar;
    private Button btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle("Login");

        etCodigo = (EditText) findViewById(R.id.et_codigo);
        etConfirmar = (EditText) findViewById(R.id.et_confirma);
        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Validacao.testarCodigo(etCodigo.getText().toString()) ||
                        !Validacao.testarCodigo(etConfirmar.getText().toString())){
                    Toast.makeText(
                            getApplicationContext(),
                            "Codigos Inválidos",
                            Toast.LENGTH_SHORT).show();
                    }
                    else{

                        if(Validacao.confirmarCodigo(etCodigo.getText().toString(),
                                etConfirmar.getText().toString())) {
                            Gson g = new GsonBuilder()
                                    .registerTypeAdapter(User.class, new UserDes()).create();


                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(APIService.BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create(g))
                                    .build();

                            APIService service = retrofit.create(APIService.class);

                            Call<User> callUser = service.getUser(etCodigo.getText().toString());

                            callUser.enqueue(new Callback<User>() {
                                @Override
                                public void onResponse(Call<User> call, Response<User> response) {
                                    Log.d(GIT, "CallBack Result: " + call +" / "+ response);
                                    if (!response.isSuccessful()){
                                        Toast.makeText(
                                                getApplicationContext(),
                                                "Usuario não existe",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                    else{
                                        Intent it = new Intent(LoginActivity.this, HomeActivity.class);
                                        String codigo = etCodigo.getText().toString();
                                        it.putExtra("codigo", codigo);
                                        startActivity(it);
                                    }
                                }

                                @Override
                                public void onFailure(Call<User> call, Throwable t) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            "Error" + t.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        else{
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Codigos não conferem!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
            }
        });

    }
}
